import Vue from "vue";
import Router from "vue-router";
// import store from "@/store/";

Vue.use(Router);

let router = new Router({
    // mode: "history",
    scrollBehavior() {
        return window.scrollTo({ top: 0, behavior: 'smooth' });
    },
    routes: [

        // Dashboards
        // {
        //     path: '/',
        //     name: 'analytics',
        //     component: () => import('../DemoPages/Dashboards/Product.vue'),
        // },
        //Master LED
        {
            path: '/ledlist/',
            name: 'led-list',
            component: () => import('../Views/LED/LEDList.vue'),
        },
        {
            path: '/ledlist/:dataState?',
            name: 'led-create',
            props: true,
            component: () => import('../Views/LED/LEDCreate.vue'),
        },
        {
            path: "/ledlist/:dataState/:Code?",
            name: "led-edit",
            props: true,
            component: () => import("@/Views/LED/LEDCreate.vue")
        },
       //Master Video
       {
        path: '/videolist/',
        name: 'video-list',
        component: () => import('../Views/Video/VideoList.vue'),    
       },
       //Master Playlist
       {
        path: '/playlistlist/create/:dataState?',
        name: 'playlist-create',
        props: true,
        component: () => import('../Views/Playlist/PlaylistCreate.vue'),    
       },
       {
        path: '/playlistlist/edit/:playlistCode?',
        name: 'playlist-edit',
        props: true,
        component: () => import('../Views/Playlist/PlaylistCreate.vue'),    
       },
       {
        path: '/playlistlist/',
        name: 'playlist-list',
        component: () => import('../Views/Playlist/PlaylistList.vue'),    
       },
       //DailyReport
       {
        path: '/dailyReport/dataplanusage',
        name: 'daily-dataplan',
        component: () => import('../Views/DailyReport/DataplanUsage.vue'),    
       },
       {
        path: '/dailyReport/statusmonitoring',
        name: 'daily-statusmonitoring',
        component: () => import('../Views/DailyReport/StatusMonitoringList.vue'),    
       },
       {
        path: '/dailyReport/uptimemonitoring',
        name: 'daily-uptimemonitoring',
        component: () => import('../Views/DailyReport/UptimeMonitoring.vue'),    
       },
       //HistoryReport
       {
        path: '/historyReport/dataplanusage',
        name: 'history-dataplan',
        component: () => import('../Views/HistoryReport/DataplanUsage.vue'),    
       },
       {
        path: '/historyReport/statusmonitoring',
        name: 'history-statusmonitoring',
        component: () => import('../Views/HistoryReport/StatusMonitoringList.vue'),    
       },
       {
        path: '/historyReport/uptimemonitoring',
        name: 'history-uptimemonitoring',
        component: () => import('../Views/HistoryReport/UptimeMonitoring.vue'),    
       },
       //LEDREALTIMECONTROLLER
       {
        path: '/ledcontroller',
        name: 'led-realtime-controller',
        component: () => import('../Views/LEDRealTimeController/LEDRealTimeController.vue'),    
       },
       //User Management
       {
        path: '/usermanagement',
        name: 'user-management',
        component: () => import('../Views/UserManagement/UserManagement.vue'), 
       },
       //LED Maintenance List
       {
        path: '/ledmaintenancelist/',
        name: 'led-maintenance-list',
        component: () => import('../Views/Maintenance/OnGoingMaintenanceList.vue'),
        },
       //Institutions
       {
        path: '/InstitutionsManagement',
        name: 'InstitutionsManagement',
        component: () => import('../Views/institutionManagement/institutionManagement.vue'), 
       },
       {
        path: '/InstitutionsManagement/create/:dataState?',
        name: 'add-institutions',
        props: true,
        component: () => import('../Views/institutionManagement/institutionCreate.vue'), 
       },
       //RoleManagement
       {
        path: '/RoleManagement',
        name: 'RoleManagement',
        component: () => import('../Views/RoleManagement/RoleManagement.vue'), 
       },
       //organizationcategory
       {
        path: '/organization-category-management',
        name: 'organization-category-management',
        component: () => import('../Views/OrganizationCategoyManagement/OrganizationCategory.vue'), 
       },
       {
        path: '/organization-category-management/create/:dataState?',
        name: 'add-Category',
        props: true,
        component: () => import('../Views/OrganizationCategoyManagement/CategoryCreate.vue'),
        },
        //User_management
        {
            path: '/user_management',
            name: 'user_management',
            component: () => import('../Views/User_Management/User_Management.vue'), 
        },
        {
            path: '/user_management/create/:dataState?',
            name: 'add-user_management',
            props: true,
            component: () => import('../Views/User_Management/User_ManagementCreate.vue'),
        },
        //Address_management
        {
            path: '/AddressManagement',
            name: 'AddressManagement',
            component: () => import('../Views/AddressManagement/AddressManagement.vue'), 
        },
        {
            path: '/AddressManagement/create/:dataState?',
            name: 'add-AddressManagement',
            props: true,
            component: () => import('../Views/AddressManagement/AddressManagementCreate.vue'),
        },
        //Service Skill Management
        {
        path: '/serviceskillmanagement',
        name: 'service-skill-management',
        component: () => import('../Views/ServiceSkillManagement/ServiceSkillManagement.vue'), 
        },
        {
            path: '/serviceskillmanagement/:dataState?',
            name: 'service-skill-create',
            props: true,
            component: () => import('../Views/ServiceSkillManagement/ServiceSkillCreate.vue'),
        },
        //Service Package Management
        {
        path: '/servicepackagemanagement',
        name: 'service-package-management',
        component: () => import('../Views/ServicePackageManagement/ServicePackageManagement.vue'), 
        },
        {
            path: '/servicepackagemanagement/:dataState?',
            name: 'service-package-create',
            props: true,
            component: () => import('../Views/ServicePackageManagement/ServicePackageCreate.vue'),
        },
        //Elderly Management
        {
        path: '/elderlymanagement',
        name: 'elderly-management',
        component: () => import('../Views/ElderlyManagement/ElderlyManagement.vue'), 
        },
        {
            path: '/elderlymanagement/:dataState?',
            name: 'elderly-create',
            props: true,
            component: () => import('../Views/ElderlyManagement/ElderlyCreate.vue'),
        },
        {
            path: '/elderlymanagement/:dataState?',
            name: 'elderly-edit',
            props: true,
            component: () => import('../Views/ElderlyManagement/ElderlyEdit.vue'),
        },
        
        //Institutional Managemen Personel
        {
            path: '/institutionmanagementpersonel',
            name: 'institution-management-personel',
            component: () => import('../Views/institutionManagementPersonel/institutionManagementPersonel.vue'), 
            },
            {
                path: '/institutionmanagementpersonel/:dataState?',
                name: 'institution-management-personel-create',
                props: true,
                component: () => import('../Views/institutionManagementPersonel/institutionManagementPersonelCreate.vue'),
            },
        //Workorder
        {
            path: '/workOrderManagement',
            name: 'workOrder-Management',
            component: () => import('../Views/workOrderManagement/workOrderManagement.vue'), 
            },
            {
                path: '/workOrderManagement/:dataState?',
                name: 'workOrder-Management-create',
                props: true,
                component: () => import('../Views/workOrderManagement/workOrderManagementCreate.vue'),
            },
    ]
})

// router.beforeEach((to, from, next) => {
//     if (to.name !== "user-login" && !store.state.auth.isAuthenticated) {
//         next({ name: "user-login" });
//     } else {
//         next();
//     }
// })

export default router;