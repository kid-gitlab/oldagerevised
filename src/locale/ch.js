export default {
    "nextLED":"下一个LED - 您的视力提供者",
    "hello": "大家好",
    "chineseMap":"中文地图",
    "chinese":"中文",
    "desc":"描述",
    "save":"保存",
    "isActive": "活性",
    "Actions":"动作",
    "Delete":"删除",
    "Height":"高度",
    "Width": "宽度",
    "Orientation":"取向",
    "FileUpload":"上传文件",
    "NoFileChoosen":"没有选中任何文件",
    "Create":"创建",
    "Edit":"编辑",
    "Cancel":"取消",
    "Refresh":"刷新",
    "UnderMaintenance":"正在维修",
    "Uptime":"正常运行时间",
    "AreYouSureDelete":"您确定要删除吗？",
    "Status":"状态",
    "Center":"中央",
    "Inquire":"查詢",
    "Reset":"重啟",
    "Add To":"添加",
    "Determine":"確定",
    //MENU
    "LEDList": "LED清单",
    "advList":"广告媒体管理",
    "playlistList": "播放清单管理",
    "dailyMon": "日常运作监控",
      "statusMon": "状态监控",
      "uptimeMon": "正常运行时间监控",
      "dataPlanMon": "数据计划监控",
      "maintenanceHistory":"维修记录",
    "LEDReal":"LED实时控制器",
    "onGoingMain":"进行中的维护",
    "userManagement": "用户管理",
    "historyRep":"历史报告",
    //LED List
    "newLED": "新LED",
    "LEDCreate":"LED创建",
    "LEDDetail":"LED细节",
    "buildNumber":"内部编号",
    "Longitude":"经度",
    "Latitude":"纬度",
    "Region":"区域",
    "Country":"国家",
    "Dimension":"尺寸",
    "Landscape":"景观",
    "Portrait":"肖像",
    "Schedule":"时间表",
    "WeekOfDay":"星期几",
    "Playlist":"播放清单",
    "StatusMaintenance":"状态维护",
    "Monday":"星期一",
    "Tuesday":"星期二",
    "Wednesday":"星期三",
    "Thursday":"星期四",
    "Friday":"星期五",
    "Saturday":"星期六",
    "Sunday":"星期日",
    // Video
    "Video":"视频",
    "UploadNewVideo":"上载新影片",
    "LocationURL":"位置网址",
    //Playlist
    "NewPlaylist":"新播放列表",
    "AddMedia":"添加媒体",
    "Sequence":"序列",
    "SelectMedia":"选择媒体",
    "CreatePlaylist":"创建播放列表",
    "EditPlaylist":"编辑播放列表",
    //Report
    "StatusMonitoring":"状态监控",
    "DataPlanUsage":"数据计划的使用",
    "LEDOnGoingMaintenance":"LED进行维护",
    "Name":"名称",
    "NetworkStatus":"网络状态",
    "DowntimeStatus":"停机状态",
    "UptimeStatus":"正常运行状态",
    "SIMCard":"SIM卡",
    "DataUsage":"数据使用",
    //UserManagement
    "UserManagement":"用户基础信息",
    "NewUser":"用户基础信息",
    "Username":"用户名",
    "Email":"电子邮件",
    "Password":"密码",
    "ConfirmPassword":"确认密码",
    "ChooseRole":"选择角色",
    "PhoneNumber":"手机号码",
    "Userdescription":"用户说明",
    "otherUserInfo":"其他信息",
    "Usergender":"用户性别",
    "Userage":"用户年龄",
    "Dateofbirth":"出生年月",
    "Hometown":"籍贯",
    "Nation":"民族",
    "Actualresidence":"实际居住地",
    "Othernumber":"其他号码",
    //Service Skill Management
    "ServiceSkillManagement":"服務技能管理",
    "ServiceSkillInformation":"服務技能信息",
    "ServiceCategorySelection":"服務類別選擇",
    "PleaseEnterQueryKeywords":"請輸入查詢關鍵詞",
    "SupportFillColumnSearch":"支持填充列搜索",
    "ServiceSkillList":"服務技能清單",
    "ServiceType":"服務類型",
    "ServiceItems":"服務項目",
    "DetailedStandardDescription":"詳細標准說明",
    "PreparedBy":"編制",
    "DateOfOrder":"訂購日期",
    "BillingStaff":"計費人員",
    //Institutions
    "addInstitution":"机构信息",
    "ListOfInstitutions":"List Of Institutions",
    "establishDate":"成立日期",
    "legalperson":"Legal Person",
    "inputusername":"Input Username",
    "billingdate":"Billing Date",
    "inputdate":"Input Date",
    "institutionscategory":"机构类别",
    "Administrativearea":"行政区域归属",
    "Institutionsname":"机构名称",
    "Institutionalattributes":"机构属性",
    "corporation":"法人",
    "Personincharge":"机构负责人",
    "contactnumber":"联系电话",
    "Institutionlocation":"机构所在地",
    "organdesc":"机构描述",
    "Institutionalservicearea":"机构服务区域",
    //ADD NURSING WORKER
    "Newandrevisedrecruitmentinformationfornursingworkers":"护工招聘信息新增、修改",
    "Affiliation":"所属机构",
    "Recruitmentstatus":"招聘状态",
    "Recruitment":"招聘人数",
    "positionName":"职位名称",
    "genderRequirement":"职位名称",
    "Salaryrange":"薪资范围",
    "workplace":"工作地点",
    "Jobdescription":"职位说明",
    //workorder
    "WorkOrderAcceptanceInformation":"工单受理信息",
    "WorkOrderNumber":"工单编号",
    "Elderlyname":"老人姓名",
    "Callernumber":"来电号码",
    "ServicePackage":"服务套餐",
    "Calltime":"来电时间",
    "servicerequirements":"服务需求",
    "Actualresidence":"实际居住地",
    //OrgannizationCategory
    "ListofInstitutionalCategories":"机构类别列表",
    "newOrganizationCategory":"添加",
    "categoryCreate":"机构类别信息",
    "orgaCategorry":"机构类别",
    "uploadImg":"点击上传",
    "Detaileddescription":"详细说明",
    "Iconupload":"图标上传",
    "Billingstaff":"制单人员",
    "Dateoforder":"制单日期",
    //addressManagement
    "addressCreate":"地址基础信息",
    "City":"城市",
    "District":"区/县",
    "Street":"街道/镇",
    "Community":"社区/村",
    //RoleManagement
    "RoleManagement":"角色列表",
    //Service Skill Management
    "ServiceSkillManagement":"服務技能管理",
    "ServiceSkillInformation":"服務技能信息",
    "ServiceCategorySelection":"服務類別選擇",
    "PleaseEnterQueryKeywords":"請輸入查詢關鍵詞",
    "SupportFillColumnSearch":"支持填充列搜索",
    "ServiceSkillList":"服務技能清單",
    "ServiceType":"服務類型",
    "ServiceItems":"服務項目",
    "DetailedStandardDescription":"詳細標准說明",
    "PreparedBy":"編制",
    "DateOfOrder":"訂購日期",
    "BillingStaff":"計費人員",
    //Service Package Management
    "ServicePackageManagement":"服務包管理",
    "ServicePackageList":"服務包清單",
    "PackageName":"包裹名字",
    "PackageNotes":"包裝說明",
    "PackageStatus":"包裝狀態",
    "ServiceType":"服務類型",
    "ServiceItems":"服務項目",
    "EstimatedDuration":"預計時間",
    "ServiceNotes":"服務須知",
    "Enabled":"已啟用",
    "AddServicePackage":"添加服務包",
    "ServicePackageName":"服務包名稱",
    "Operating":"操作",
    "AddServiceItems":"添加服務項目",
    //LEDController
    "Online":"线上",
    "Offline":"离线",
    "DevicePlaylist":"设备播放列表",
    "MediaOperation":"媒体运作",
    "NowPlaying":"正在播放",
    "PlaylistDescription":"播放清单说明",
    "MediaList":"媒体清单",
    "ChangePlaylist":"更改播放列表",
  };
  